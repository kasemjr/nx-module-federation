import { getTitle } from '../support/home.po';

describe('app-one', () => {
  beforeEach(() => cy.visit('/'));

  it('should display the title in page', () => {
    getTitle().contains('MFE App One Home');
  });
});
