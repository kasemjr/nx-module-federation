import { Component } from '@angular/core';

@Component({
  selector: 'redbarba-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {}
