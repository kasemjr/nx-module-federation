import { Component } from '@angular/core';

@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent {
  title = 'MFE App One Home';
}
