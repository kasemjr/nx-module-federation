import { Component } from '@angular/core';

@Component({
  selector: 'redbarba-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'shell';
}
